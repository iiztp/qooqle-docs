import {Router} from "express";
import io from "../server.js";

const router = new Router();

let calendar = [];

router.get("", (req, res) => {
    res.send("Hello world!");
});

router.get("/:calendarName", (req, res) => {
    let name = req.params.calendarName;
    if(calendar[name] === undefined) {
        calendar[name] = [];
    }
    res.send(calendar[name]);
});

router.post("/:calendarName", (req, res) => {
    let name = req.params.calendarName;
    let value = req.body.value;
    let date = new Date(req.body.year, req.body.month-1, req.body.day, req.body.hours, req.body.minutes);
    let duration = new Date(date.getTime() + req.body.duration*60000);

    if(req.body.duration <= 0 || date.toString() === "Invalid Date" || duration.toString() === "Invalid Date" || value === undefined) {
        res.status(400).send("Failure: Request must contain day (int), month (int), year (int), hours (int), minutes (int), duration (uint) and value (string)");
        return;
    }

    if(!Array.isArray(calendar[name])) {
        calendar[name] = [];
    }
    
    calendar[name].push({
        date: date.toString(),
        duration: duration.toString(),
        value: value,
    });
    
    res.send(`Success : ${date.toLocaleString("FR-fr")} to ${duration.toLocaleString("FR-fr")} : ${value}`);
    io.to(name).emit('updateCalendar');
});


router.delete("/:calendarName/:idtask", (req, res) => {
    let name = req.params.calendarName;
    let idTask = req.params.idtask;
    let taskToRemove;
    try {
        taskToRemove = calendar[name][idTask];
    } catch(err) {
        res.status(400).send("Failure: Task with id " + idTask + " doesn't exist");
        return;
    }
    calendar[name].splice(taskToRemove, 1);
    res.send(`Success : Task deleted`);
    io.to(name).emit('updateCalendar')
});

router.patch("/:calendarName/:idtask", (req, res) => {
    let name = req.params.calendarName;
    let idTask = req.params.idtask;
    let taskToUpdate;
    
    try {
        taskToUpdate = calendar[name][idTask];
    }catch(err){
        res.status(400).send("Failure: Task with id " + idTask + " doesn't exist");
        return;
    }

    if(req.body.duration && req.body.duration <= 0) {
        res.status(400).send("Duration must be uint");
        return;
    }
  
    let taskDate = new Date(taskToUpdate.date);
    let date = new Date(
        (req.body.year) ? req.body.year : taskDate.getFullYear(),
        (req.body.month) ? req.body.month-1 : taskDate.getMonth(),
        (req.body.day) ? req.body.day : taskDate.getDate(),
        (req.body.hours) ? req.body.hours : taskDate.getHours(),
        (req.body.minutes) ? req.body.minutes : taskDate.getMinutes());
    let value = (req.body.value) ? req.body.value : taskToUpdate.value;
    let duration = new Date(date.getTime() + ((req.body.duration) ? req.body.duration*60000 : (new Date(taskToUpdate.duration)).getTime()-taskDate.getTime()));

    taskToUpdate.date = date.toString();
    taskToUpdate.value = value;
    taskToUpdate.duration = duration.toString();
    res.send(`Success : Task ${idTask} was updated successfully`);
    io.to(name).emit('updateCalendar')
});


export default router