let listMeetings = [];
const socket = io();
const params = new URL(document.location).searchParams;
let calendarName = params.get("name");
let renderFunction = renderMonth;
actualize();

socket.emit('join', calendarName);
socket.on('updateCalendar', () => actualize());

/**
 * Actualise l'agenda actuel.
 */
function actualize() {
    axios.get(`/api/${calendarName}`).then((response) => {
        listMeetings = [];
        response.data.forEach((element, key) => {
            element.id = key;

            /**
             * Valeur de color à changer (random ou autre) pour pouvoir différencier les meetings
             */
            element.color = generateColor(key);
            let date = new Date(element.date);
            date.setHours(0, 0, 0, 0);
            let dateStr;
            let elementCopy;
            for (let i = date; i <= new Date(element.duration); i.setDate(i.getDate() + 1)) {
                dateStr = i.toLocaleDateString();
                elementCopy = Object.assign({}, element);
                if(!listMeetings[dateStr]) {
                    elementCopy.nbCollisions = elementCopy.posCollision = 0;
                    listMeetings[dateStr] = [elementCopy];
                } else {
                    detectMeetingsCollisions(elementCopy, listMeetings[dateStr])
                    listMeetings[dateStr].push(elementCopy);
                }
            }
        });
        renderFunction(currentDateShown);
    });
}


const meetingColors = ['#3B8AC3','#1C6E7D','#00C5C9','#0ECF76','#00AF90','#C34B8B','#FF865E','#00A8AA','#649C84','#00A1FF'];
function generateColor(index){
    if (index >= meetingColors.length){
        let randomColor = Math.floor(Math.random()*16777215).toString(16);
        return '#'+randomColor;
    }
    return meetingColors[index];
}
/**
 * Fonction de détection de collision de meetings
 */
function detectMeetingsCollisions(meeting, others) {
    let res = 0;
    for (const other of others) {
        if (new Date(meeting.date) < new Date(other.duration)
            && new Date(meeting.duration) > new Date(other.date)) {
            res++;
            other.nbCollisions++;
        }
    }
    meeting.nbCollisions = meeting.posCollision = res;
}

/**
 * Fonction iso8601Week du widget Datepicker de Jquery UI (un peu modifiée.
 * https://api.jqueryui.com/datepicker/#option-calculateWeek
 */
Date.prototype.getWeek = function () {
    let time, checkDate = new Date(this.getTime());
    checkDate.setDate( checkDate.getDate() + 4 - checkDate.getDay() );
    time = checkDate.getTime();
    checkDate.setMonth( 0 );
    checkDate.setDate( 1 );
    return Math.floor( Math.round( ( time - checkDate ) / 86400000 ) / 7 ) + 1;
}

window.onload = () => {
    document.title = (calendarName || "") + " - Syncal"
}


/** Fonctionnalité d'ajout d'un rendez-vous. */
const addMeetingDiv = document.getElementById('add-meeting');
function addMeeting(event) {
    event.preventDefault();
    addMeetingDiv.classList.add('hidden');
    divWindow.classList.add('hidden');
    let form = event.target;
    let startDate = new Date(form["start-date"].value);
    let endDate = new Date(form["end-date"].value);
    let duration = (endDate-startDate)/60000;

    axios.post(`/api/${calendarName}`, {
        value: form["value"].value,
        day: startDate.getDate(),
        month: startDate.getMonth()+1,
        year: startDate.getFullYear(),
        hours: startDate.getHours(),
        minutes: startDate.getMinutes(),
        duration: duration,
    });
}


function showAddMeeting() {
    addMeetingDiv.classList.remove('hidden');
    showDivWindow(() => addMeetingDiv.classList.add("hidden"));
}


/** Fonctionnalité de modification d'un rendez-vous. */
const modifyMeetingDiv = document.getElementById('modify-meeting');
const form = document.getElementById('modify-meeting-form');
let meetingToUpdate;
function onModifyMeeting(meeting){
    btnClose.click();
    showDivWindow(() => modifyMeetingDiv.classList.add('hidden'));
    form["value"].value = meeting.value;
    const date = new Date(meeting.date);
    date.setHours(date.getHours() - date.getTimezoneOffset() / 60);
    form["start-date"].value = date.toISOString().substring(0, 16);
    const duration = new Date(meeting.duration);
    duration.setHours(duration.getHours() - duration.getTimezoneOffset() / 60);
    form["end-date"].value = duration.toISOString().substring(0,16)
    form["end-date"].min = date.toISOString().substring(0, 16);
    modifyMeetingDiv.classList.remove('hidden');
    meetingToUpdate = meeting;
}
function modifyMeeting(event) {
    event.preventDefault();
    modifyMeetingDiv.classList.add('hidden');
    divWindow.classList.add('hidden');
    let startDate = new Date(form["start-date"].value);
    let endDate = new Date(form["end-date"].value);
    let duration = (endDate-startDate)/60000;
    axios.patch(`/api/${calendarName}/${meetingToUpdate.id}`, {
        value: form["value"].value,
        day: startDate.getDate(),
        month: startDate.getMonth()+1,
        year: startDate.getFullYear(),
        hours: startDate.getHours(),
        minutes: startDate.getMinutes(),
        duration: duration,
    });
}

function deleteMeeting(meeting) {
    axios.delete(`/api/${calendarName}/${meeting.id}`);
}

const navDiv = document.getElementById("nav");

let currentDateShown = new Date();
const agendaDiv = document.getElementById("agenda-view");
const monthName = document.getElementById("month-name");
function createDayDiv(day, meetings) {
    const dayDiv = document.createElement("div");
    dayDiv.classList.add("day");
    if (day) {
        const dayNumber = document.createElement("div");
        dayNumber.classList.add("day-number");
        dayNumber.append(day);
        dayDiv.appendChild(dayNumber);
        dayDiv.addEventListener("click", showDayMeetings);

        if (meetings.length) {
            for (const meeting of meetings) {
                const meetingSpan = document.createElement("span");
                const p = document.createElement("p");
                p.textContent = meeting.value;
                p.title = meeting.value;
                meetingSpan.appendChild(p);
                meetingSpan.classList.add("meeting-span");
                const deleteSpan = document.createElement("span");
                deleteSpan.className = "icon material-icons-round md-18";
                deleteSpan.textContent = "delete";
                deleteSpan.onclick = (evt) => {
                    evt.stopPropagation();
                    deleteMeeting(meeting);
                }
                meetingSpan.appendChild(deleteSpan);
                meetingSpan.onclick = (evt) => {
                    evt.stopPropagation();
                    onModifyMeeting(meeting);
                }
                meetingSpan.style.background = meeting.color;
                dayDiv.appendChild(meetingSpan);
            }
        } else {
            const noMeeting = document.createElement("div");
            noMeeting.textContent = "Une journée bien vide...";
            noMeeting.classList.add("no-meeting");
            dayDiv.appendChild(noMeeting);
        }

    } else dayDiv.classList.add("empty");

    const div = document.createElement("div");
    div.appendChild(dayDiv);
    return div;
}
function renderMonth(date) {
    monthName.innerText = date.toLocaleString("default", {month: "long"}).toUpperCase() + " " + date.getFullYear();

    let month = calendarize(date, 1);
    if (month.length<6) month.push([0, 0, 0, 0, 0, 0, 0]);

    agendaDiv.classList.remove("week-view");
    agendaDiv.classList.remove("day-view");

    const tmpDate = new Date(date);
    agendaDiv.textContent = "";
    for (const week of month) {
        let weekDiv = document.createElement("div");
        weekDiv.classList.add("week");
        const weekNumber = document.createElement("span");
        weekNumber.classList.add("week-number");
        weekDiv.appendChild(weekNumber);
        let meetings;
        for (const day of week) {
            meetings = [];
            if (day) {
                tmpDate.setDate(day);
                if (!weekNumber.textContent)
                    weekNumber.textContent = tmpDate.getWeek().toString();
                meetings = listMeetings[tmpDate.toLocaleDateString()] || [];
            }
            weekDiv.appendChild(createDayDiv(day, meetings));
        }
        agendaDiv.appendChild(weekDiv);
    }
}
function gotoToday() {
    currentDateShown=new Date();
    renderFunction(currentDateShown);
}

const btnClose = document.getElementById("btn-close");
let fnSetbackDayNumber;
let currentDayDiv;

function showDayMeetings(evt) {
    const dayDiv = evt.currentTarget;
    dayDiv.classList.add("zoomed");
    btnClose.classList.remove("hidden");
    dayDiv.appendChild(btnClose);
    divWindow.classList.remove("hidden");
    divWindow.onclick = () => btnClose.click();
    const dayNumber = dayDiv.querySelector(".day-number").textContent;
    dayDiv.querySelector(".day-number").textContent =
        new Date(currentDateShown.getFullYear(), currentDateShown.getMonth(), dayNumber).toLocaleDateString();
    fnSetbackDayNumber = () => {
        dayDiv.querySelector(".day-number").textContent = dayNumber;
    }
    dayDiv.removeEventListener("click", showDayMeetings);
    currentDayDiv = dayDiv;
}

btnClose.onclick = evt => {
    evt.stopPropagation();
    try {
        fnSetbackDayNumber();
        btnClose.parentElement.classList.remove("zoomed");
        btnClose.classList.add("hidden");
        divWindow.classList.add("hidden");
        document.body.appendChild(btnClose);
        currentDayDiv.addEventListener("click", showDayMeetings);
    } catch (e) {}
}

/** Fonctionnalité d'affichage par semaine */
const milliInDay = 864000;
const hoursDiv = document.createElement("div")
hoursDiv.className = "hours"
const gap = 24 / Math.floor(window.innerHeight / 150)
for (let i = gap; i < 24; i+=gap) {
    const hour = document.createElement("span")
    hour.className = "hour-name"
    hour.style.top = "calc(" + (i * 100 / 24) + "% - 0.5em)"
    hour.textContent = i.toFixed(0) + "h"

    const hr = document.createElement("hr")
    hr.className = "hour-line"
    hr.style.top = (i * 100 / 24) + "%"

    hoursDiv.append(hour, hr)
}


function renderWeek(date) {
    navDiv.querySelector("#month-name").textContent = "SEMAINE "+date.getWeek();

    agendaDiv.classList.remove("day-view");
    agendaDiv.classList.add("week-view");

    const view = document.createElement("div")
    view.className = "week-view-div"
    agendaDiv.replaceChildren(hoursDiv, view)

    const daynames = document.createElement("div")
    daynames.className = "daynames"

    const meetingsDiv = document.createElement("div")
    view.append(daynames, meetingsDiv);
    meetingsDiv.className = "wv-meetings"
    let meetings = [];

    const tmpDate = new Date(date);
    tmpDate.setDate(tmpDate.getDate() - tmpDate.getDay() +1);
    tmpDate.setHours(0,0,0,0)
    for (let i = 0; i < 7; i++) {
        const dayDiv = document.createElement("div");
        dayDiv.classList.add("day-weekview");
        const h4 = document.createElement("h4");
        h4.classList.add("day-name");
        h4.textContent = tmpDate.toLocaleString("default", {weekday: "long"}).toUpperCase();
        const div = document.createElement("div");
        div.textContent = tmpDate.toLocaleDateString();
        h4.append(div)
        daynames.appendChild(h4);
        meetings = listMeetings[tmpDate.toLocaleDateString()] || [];
        createMeetingsSpans(meetings, tmpDate, dayDiv);
        meetingsDiv.appendChild(dayDiv)
        tmpDate.setDate(tmpDate.getDate()+1)
    }
}


/** Fonctionnalité d'affichage par jour */
function renderDay(date) {
    navDiv.querySelector("#month-name").textContent = date.toLocaleString("default", {weekday: "long"}).toUpperCase() +
        " " + date.toLocaleDateString();
    agendaDiv.classList.remove("week-view");
    agendaDiv.classList.add("day-view");

    const view = document.createElement("div")
    view.className = "day-view-div"

    agendaDiv.replaceChildren(hoursDiv, view)
    const meetingsDiv = document.createElement("div")
    view.append( meetingsDiv);
    meetingsDiv.className = "dv-meetings"
    let meetings = [];
    const tmpDate = new Date(date);

    tmpDate.setHours(0,0,0,0)
    const dayDiv = document.createElement("div");
    dayDiv.classList.add("day-dayview");

    //h4 vide
    meetings = listMeetings[tmpDate.toLocaleDateString()] || [];
    createMeetingsSpans(meetings, tmpDate, dayDiv);
    meetingsDiv.appendChild(dayDiv)

}


function createMeetingsSpans(meetings, date, divAppendTo) {
    for (const meeting of meetings) {
        const meetingSpan = document.createElement("span");
        const p = document.createElement("p");
        p.textContent = meeting.value;
        p.title = meeting.value;
        meetingSpan.appendChild(p);
        meetingSpan.classList.add("meeting-span");
        meetingSpan.onclick = (evt) => {
            evt.stopPropagation();
            onModifyMeeting(meeting);
        }

        const dateMeeting = new Date(meeting.date);
        const duration = new Date(meeting.duration);

        let posTop = (dateMeeting.getTime() - date.getTime()) / milliInDay
        if (posTop < 0) posTop = 0;
        meetingSpan.style.top = posTop + "%"

        let posBottom = (duration.getTime() - date.getTime()) / milliInDay
        if (posBottom > 100) posBottom = 100;
        meetingSpan.style.bottom = 100 - posBottom + "%"

        const width = 100 / (meeting.nbCollisions+1)
        meetingSpan.style.width = `calc(${width}% - 6px)`
        meetingSpan.style.left = (meeting.posCollision * width) + "%"
        meetingSpan.style.backgroundColor = meeting.color;

        const deleteSpan = document.createElement("span");
        deleteSpan.className = "icon material-icons-round md-18";
        deleteSpan.textContent = "delete";
        deleteSpan.onclick = (evt) => {
            evt.stopPropagation();
            deleteMeeting(meeting);
        }
        meetingSpan.appendChild(deleteSpan);
        divAppendTo.appendChild(meetingSpan);
    }
}

/** Changement vue par mois/par semaine */
function changeMonth(offset) {
    currentDateShown.setMonth(currentDateShown.getMonth() + offset);
    renderMonth(currentDateShown);
}
function changeWeek(offset) {
    currentDateShown.setDate(currentDateShown.getDate() + 7*offset);
    renderWeek(currentDateShown);
}
function changeDay(offset) {
    currentDateShown.setDate(currentDateShown.getDate() + offset);
    renderDay(currentDateShown);
}
const navLeft = document.getElementById("nav-left");
const navRight = document.getElementById("nav-right");
const indicator = document.getElementById("indicator");
const MODE_WEEK = 0, MODE_MONTH = 1, MODE_DAY = 2;
function changeView(viewMode) {
    agendaDiv.textContent = "";
    localStorage.setItem("view-mode", viewMode);

    let changeFn;
    switch (viewMode) {
        case MODE_MONTH:
            renderFunction = renderMonth;
            changeFn = changeMonth;
            indicator.className = "";
            break;
        case MODE_WEEK:
            renderFunction = renderWeek;
            changeFn = changeWeek;
            indicator.className = "middle";
            break;
        case MODE_DAY:
            renderFunction = renderDay;
            changeFn = changeDay;
            indicator.className = "right";
            break;
    }
    renderFunction(currentDateShown);
    navLeft.onclick = () => changeFn(-1);
    navRight.onclick = () => changeFn(1);
}
if (localStorage.getItem("view-mode") !== MODE_MONTH.toString()) {
    changeView(Number(localStorage.getItem("view-mode")))
}



function changeMinEndDateModify(){
    changeMinEndDate(document.getElementById('modify-meeting-form'));
}

function changeMinEndDateAdd(){
    changeMinEndDate(document.getElementById('add-form'));
}

function changeMinEndDate(form){
    let inputAddDate = form["start-date"];
    let inputEndDate = form["end-date"];
    inputEndDate.setAttribute("min", inputAddDate.value);
}

/**
 * Fond blur et gris pour les fenêtres avec un formulaire, etc.
 */

const divWindow = document.getElementById("div-window");
function showDivWindow(callback) {
    divWindow.classList.remove("hidden");
    divWindow.onclick = () => {
        divWindow.classList.add("hidden");
        callback(); // callback pour fermer la fenetre ouverte lors du click à l'extérieur de celle-ci.
    }
}