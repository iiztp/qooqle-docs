import express from "express";
import http from "http";
import {Server} from "socket.io";
import api from "./routes/api.js";

const app = express();
const server = http.createServer(app);
const port = 3000;
const io = new Server(server);

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/api", api)
app.use(express.static("public"));

app.all("*", (req, res) => {
    res.redirect("/error.html");
})

server.listen(port, () => console.log(`Running on http://localhost:${port}`));

io.on('connection', (socket) => {
    socket.on('join', (msg) => socket.join(msg))
});

export default io;