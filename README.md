# Syncal

Utilisation de websockets pour la synchronisation des clients.


Sauvegarde des tâches dans la mémoire vive.

## Serveur : Installation et lancement

`npm i` puis `node server.js`

## Client

Aller à l'adresse `http://localhost:3000`


Spécifier un `nom de calendrier`


Pour ajouter une tâche -> Bouton en haut à droite, spécifie les renseignements

## API

host : http://localhost:3000/api/

### GET /:calendarName

:calendarName le nom du calendrier à récupérer les tâches.

#### Retour

200


Tableau de tâches.


tâche : {


    name: string,


    value: number,


    date: Date,


    duration: number


}

#### Exemple de requête

`curl -X GET http://localhost:3000/api/:calendarName/`

### POST /:calendarName : day, month, year, hours, minutes, duration, value

:calendarName le nom du calendrier

#### Retour

200 si tout s'est bien passé.


400 sinon.

#### Exemple de requête

`curl -X POST -d 'day=XX&month=XX&year=XXXX&hours=XX&minutes=XX&value=...&duration=XXX' http://localhost:3000/api/:calendarName/` 


La gestion des dates invalides se fait par l'objet Date de Javascript (14 ème mois = 1 an de plus et 1 er mois).

### DELETE /:calendarName/:idTask

:calendarName le nom du calendrier 


:idTask l'id de la tâche (récupérable par GET sur le calendrier)

#### Retour

200 si tout s'est bien passé. 


400 si la tâche n'existe pas (sinon).

#### Exemple de requête

`curl -X DELETE http://localhost:3000/api/:calendarName/:idTask/`

### PATCH /:calendarName/:idTask : (opt) day, (opt) month, (opt) year, (opt) hours, (opt) minutes, (opt) duration, (opt) value


:calendarName le nom du calendrier 


:idTask l'id de la tâche (récupérable par GET sur le calendrier)

#### Retour

200 si tout s'est bien passé. 


400 si la tâche n'existe pas (sinon).

#### Exemple de requête

`curl -X PATCH -d 'day=XX' http://localhost:3000/api/:calendarName/:idTask/`